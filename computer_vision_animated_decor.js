;
var ComputerVision = (function () {
        'use strict';
        const   OBJECTS_COUNT = 14,
                FAR = 1800,
                NEAR = 0;


        var _canvas,
            _context,
            _isStarted,
            _objects,
            _isDebug,
            _width,
            _height,
            _color;


        var _draw = function () {
            // UPDATE STATE
            // DRAW
            var object, point, vPoint, points, rects, rect;
            _context.clearRect(0,0,_width,_height);
            if (_isDebug) _drawCenter();
            for (var i = 0; i < _objects.length; i++) {
                object = _objects[i];
                _context.strokeStyle = _color;
                _context.globalAlpha = Math.sin(object.z/FAR*Math.PI);
                _updateObject(object);
                if (object.dead) {
                    _objects.splice(i, 1);
                    _objects.push(_defineObject());
                    i--;
                    continue;
                }
                points = [];
                for (var j = 0; j < object.points.length; j++) {
                    point = _translateToView(object, object.points[j]);
                    points.push({'x': point.x, 'y': point.y});
                }
                rects = _pointsToRects(points);
                for (var j = 0; j < rects.length; j++) {
                    rect = rects[j];
                    _context.strokeRect(rect.x, rect.y, rect.w, rect.h);
                }
                // DEBUG DRAW
                if (_isDebug) {
                    _context.strokeStyle = '#FF0050';
                    _context.globalAlpha = 1;
                    _context.beginPath();
                    for (var j = 0; j < object.points.length; j++) {
                        point = object.points[j];
                        vPoint = _translateToView(object, point);
                        if (j === 0) {
                            _context.moveTo(vPoint.x, vPoint.y);
                        } else {
                            _context.lineTo(vPoint.x, vPoint.y);
                        }
                    }
                    _context.stroke();
                }
            }
            if (_isStarted) {
                requestAnimationFrame(_draw);
            }
        }

        var _translateToView = function(object, point) {
            return {
                'x': _width/2 + (object.x + point.x - object.center.x)*_width/2/object.z,
                'y': _height/2 + (object.y + point.y - object.center.y)*_height/2/object.z,
            }
        }

        var _pointsToRects = function (points) {
            var rects = [], p0, p1, p2, rect0, rect1, x, y, w, h, overlap, minX, maxX, minY, maxY;
            for (var i = 1; i < points.length; i++) {
                p0 = points[i-1];
                p1 = points[i];
                x = Math.min(p0.x,p1.x);
                y = Math.min(p0.y,p1.y);
                w = Math.max(p0.x,p1.x)-x;
                h = Math.max(p0.y,p1.y)-y;
                rects.push({'x':x, 'y':y, 'w':w, 'h':h});
            }
            overlap = true;
            const MIN_SIZE = 10;
            while (overlap) {
                overlap = false;
                for (var i = 1; i < rects.length; i++) {
                    rect0 = rects[i-1];
                    rect1 = rects[i];
                    x = Math.min(rect0.x, rect1.x);
                    y = Math.min(rect0.y, rect1.y);
                    w = Math.max(rect0.x+rect0.w, rect1.x+rect1.w)-x;
                    h = Math.max(rect0.y+rect0.h, rect1.y+rect1.h)-y;
                    if (w < rect0.w+rect1.w || h < rect0.h+rect1.h || rect0.w < MIN_SIZE || rect1.w < MIN_SIZE || rect0.h < MIN_SIZE || rect1.h < MIN_SIZE ) {
                        rects.splice(i-1,1);
                        rect1.x = x;
                        rect1.y = y;
                        rect1.w = w;
                        rect1.h = h;
                        overlap = true;
                        break;
                    };
                }
            }
            if (rects.length === 1) {
                rects[0].w = Math.max(rects[0].w, MIN_SIZE);
                rects[0].h = Math.max(rects[0].h, MIN_SIZE);
            }
            return rects;
        }


        var _updateObject = function (object) {
            var p0, p1,
                minX = 0,
                maxX = 0,
                minY = 0,
                maxY = 0;
                object.y += 1;
                object.z -= object.zV;
                if (object.z <= 0) {
                    object.dead = true;
                    return;
                }
            for (var i = 1; i < object.points.length; i++) {
                p0 = object.points[i-1];
                p1 = object.points[i];
                if (Math.random() > 0.99) {
                    if (Math.random() > 0.99) {
                        p1.a1 = p0.a0 + 2*ANGLE_JITTER*_r();
                    } else {
                        p1.a1 = p0.a0 + ANGLE_JITTER*_r();
                    }
                }
                p1.a0 += (p1.a1 - p1.a0)/150;
                p1.x = p0.x + p0.r*Math.cos(p1.a0);
                p1.y = p0.y + p0.r*Math.sin(p1.a0);
                minX = Math.min(minX, p1.x);
                maxX = Math.max(maxX, p1.x);
                minY = Math.min(minY, p1.y);
                maxY = Math.max(maxY, p1.y);
            }
            object.rect.x = minX;
            object.rect.y = minY;
            object.rect.w = maxX-minX;
            object.rect.h = maxY-minY;
            p0 = _translateToView(object, {'x':minX, 'y':minY});
            p1 = _translateToView(object, {'x':maxX, 'y':maxY});
            if ((p0.x < 0 || p0.x > _width || p0.y < 0 || p0.y > _height) && (p1.x < 0 || p1.x > _width || p1.y < 0 || p1.y > _height)) {
                object.dead = true;
            }
            object.center.x = object.rect.x + object.rect.w/2;
            object.center.y = object.rect.y + object.rect.h/2;
        }


        const   ANGLE_JITTER = Math.PI/4,
                BASE_ANGLE = Math.PI/4;


        var _defineObject = function () {
            var positionJitter = _r();
            var object = {
                'points':[],
                'x':-Math.max(_width, _height)/2+Math.max(_width, _height)*positionJitter,
                'y':-_height/4+_height/2*Math.random(),
                'z':FAR,
                'zV':2*(1+Math.random()),
                'center': {'x':0, 'y':0},
                'color':'#'+Math.round(Math.random()*(255*255*255)).toString(16)
            };
            var point = {'x':0, 'y':0},
                minX = 0,
                maxX = 0,
                minY = 0,
                maxY = 0,
                a, r;
            for (var i = 0; i < 2+15*Math.random()*(1-Math.abs(positionJitter)); i++) {
                if (i === 0) {
                    if (Math.random > 0.5) {
                        a = BASE_ANGLE + ANGLE_JITTER*_r();
                    } else {
                        a = -BASE_ANGLE + ANGLE_JITTER*_r();
                    }
                } else {
                    a = object.points[i-1].a0 + ANGLE_JITTER*_r();
                }
                r = Math.random() > 0.85 ? (60+Math.random()*30) : (5+Math.random()*10);
                r*= 2.4;
                object.points.push({'x':0, 'y':0, 'a0':a, 'r':r, 'a1':a});
            }
            object.rect = {'x':0, 'y':0, 'w':0, 'h':0};
            object.center.x = 0;
            object.center.y = 0;
            _updateObject(object);
            return object;
        }


        var _start = function (canvas, w, h, color) {
            if (canvas === undefined || canvas === null) {
                console.error('Wrong canvas');
                return;
            }
            if (w === undefined) w = 800;
            if (h === undefined) h = 600;
            if (color === undefined) color = '#000000';
            _width = w; _height = h; _color = color;
            if (_objects === undefined) {
                _objects = [];
                var object;
                for (var i = 0; i < OBJECTS_COUNT; i++) {
                    object = _defineObject();
                    object.z *= Math.random();
                    object.y += (FAR - object.z);
                    _objects.push(object);
                }
            }
            if (_canvas === undefined) {
                requestAnimationFrame(_draw);
            }
            _canvas = canvas;
            _context = _canvas.getContext('2d');
            _isStarted = true;
        }


        var _stop = function () {
            _isStarted = false;
            _canvas = undefined;
        }

        var _r = function() {
            return 1-2*Math.random();
        }


        var _drawCenter = function () {
            _context.beginPath();
            _context.moveTo(_width/2-5, _height/2);
            _context.lineTo(_width/2+5, _height/2);
            _context.moveTo(_width/2, _height/2-5);
            _context.lineTo(_width/2, _height/2+5);
            _context.stroke();
        }


        var _debug = function (v) {
            if (v) {
                _isDebug = true;
            } else {
                _isDebug = false;
            }
        }


        return {
            'start': _start,
            'stop': _stop,
            'debug': _debug
        };
    })();


window.requestAnimFrame = (function(){
    return    window.requestAnimationFrame       || 
              window.webkitRequestAnimationFrame || 
              window.mozRequestAnimationFrame    || 
              window.oRequestAnimationFrame      || 
              window.msRequestAnimationFrame     || 
              function(/* function */ callback, /* DOMElement */ element){
                window.setTimeout(callback, 1000 / 60);
              };
})();